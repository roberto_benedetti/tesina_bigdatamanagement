package bigdataman.tesina.dataProcessing;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.bson.types.ObjectId;
import com.mongodb.hadoop.MongoConfig;
import com.mongodb.hadoop.MongoInputFormat;
import com.mongodb.hadoop.MongoOutputFormat;
import com.mongodb.hadoop.io.BSONWritable;
import com.mongodb.hadoop.util.MongoConfigUtil;
import com.mongodb.hadoop.util.MongoTool;

public class PacketProcessingFirstStep extends MongoTool{
	
	public PacketProcessingFirstStep() throws Exception {
		System.out.println("Job 1 started");
		setConf(new Configuration());
		MongoConfigUtil.setInputFormat(getConf(), MongoInputFormat.class);
        MongoConfigUtil.setOutputFormat(getConf(), MongoOutputFormat.class);
        MongoConfig config1 = new MongoConfig(getConf());
        
        config1.setMapper(Mapper1step.class);
        config1.setReducer(Reducer1step.class);
        config1.setMapperOutputKey(CompositeGroupKey.class);
        config1.setMapperOutputValue(IntWritable.class);
        config1.setOutputKey(ObjectId.class);
        config1.setOutputValue(BSONWritable.class);
        config1.setInputURI("mongodb://localhost:27017/BDM.capturedData");
        config1.setOutputURI("mongodb://localhost:27017/BDM.intermediateResults");
	}	
}