package bigdataman.tesina.dataProcessing;

import java.util.ArrayList;

public interface FeatureExpansion {
	public double compute(ArrayList<Integer> listValues);
	public String getDescriptor();

}
