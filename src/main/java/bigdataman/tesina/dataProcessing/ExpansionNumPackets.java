package bigdataman.tesina.dataProcessing;

import java.util.ArrayList;

public class ExpansionNumPackets extends AbstractFeatExpansion{
	public ExpansionNumPackets() {
		this.descriptor="numPackets";
	}

	public double compute(ArrayList<Integer> listValues) {
		return listValues.size();
	}

}
