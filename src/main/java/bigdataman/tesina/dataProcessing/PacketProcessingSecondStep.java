package bigdataman.tesina.dataProcessing;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.bson.types.ObjectId;
import com.mongodb.hadoop.MongoConfig;
import com.mongodb.hadoop.MongoInputFormat;
import com.mongodb.hadoop.MongoOutputFormat;
import com.mongodb.hadoop.io.BSONWritable;
import com.mongodb.hadoop.util.MongoConfigUtil;
import com.mongodb.hadoop.util.MongoTool;


public class PacketProcessingSecondStep extends MongoTool{
	
	public PacketProcessingSecondStep() throws IOException{
	    System.out.println("Job 2 started");
		setConf(new Configuration());

        MongoConfigUtil.setInputFormat(getConf(), MongoInputFormat.class);
        MongoConfigUtil.setOutputFormat(getConf(), MongoOutputFormat.class);
        MongoConfig config2 = new MongoConfig(getConf());
        config2.setMapper(Mapper2step.class);
        config2.setReducer(Reducer2step.class);
        config2.setMapperOutputKey(CompositeGroupKey2.class);
        config2.setMapperOutputValue(BSONWritable.class); 
        config2.setOutputKey(ObjectId.class);
        config2.setOutputValue(BSONWritable.class);
        config2.setInputURI("mongodb://localhost:27017/BDM.intermediateResults");
        config2.setOutputURI("mongodb://localhost:27017/BDM.dataset");
	}
}