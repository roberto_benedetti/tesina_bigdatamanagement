package bigdataman.tesina.dataProcessing;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.hadoop.util.ToolRunner;

import com.mongodb.hadoop.util.MongoTool;

public class PacketsProcessingMain {

	public static void main(String[] args) throws IOException, Exception {
		long startTime = System.currentTimeMillis();
		
		ArrayList<MongoTool> queue = new ArrayList<MongoTool>();
		queue.add(new PacketProcessingFirstStep());
		queue.add(new PacketProcessingSecondStep());
		for(MongoTool m:queue)
			jobScheduler(m,args);
		long stopTime = System.currentTimeMillis();
	    long elapsedTime = stopTime - startTime;
	    System.out.println("Elapsed time: " + elapsedTime +" ms");
	    }
	
	public static void jobScheduler(MongoTool tool, String[] args) throws Exception {
	    ToolRunner.run(tool, args);

	}

}