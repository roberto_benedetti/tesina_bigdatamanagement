package bigdataman.tesina.dataProcessing;

public abstract class AbstractFeatExpansion implements FeatureExpansion {
	protected String descriptor;
	
	public AbstractFeatExpansion() {
		this.descriptor="UNDEFINED";
	}
	public String getDescriptor() {
		return descriptor;
	}
}
