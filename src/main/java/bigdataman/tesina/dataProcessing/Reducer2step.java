package bigdataman.tesina.dataProcessing;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.hadoop.mapreduce.Reducer;

import org.bson.BSONObject;
import org.bson.BasicBSONObject;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.hadoop.io.BSONWritable;

public class Reducer2step  extends Reducer<CompositeGroupKey2, BSONWritable, BSONWritable, BSONWritable> {
	// Initialize the couple id - value for mongodb insert
	private BSONWritable id = new BSONWritable(); 
	private BSONWritable value = new BSONWritable(); 

	@Override	
    public void reduce(CompositeGroupKey2 key, Iterable<BSONWritable> values, Context context)
        throws IOException, InterruptedException {

        ArrayList<BSONObject> listValues = new ArrayList<BSONObject>();
        for (final BSONWritable value : values) {
        	BSONObject bson = value.getDoc();
            listValues.add(bson);
        }
        if (listValues.size()>2) {
        	System.out.println("Found 3 or more instead of max2");
        	throw new IllegalStateException("Found 3 or more bson objects, expected 1 or 2");
        }
        BSONObject outDoc = BasicDBObjectBuilder.start().add("key", key.getDBObject()).get();
        id.setDoc(outDoc);
        BasicBSONObject output = new BasicBSONObject();
        output.put("featuresOut", listValues.get(0));
        if (listValues.size()==2)
        	output.put("featuresIn", listValues.get(1));
        value.setDoc(output);
        context.write(id,value);

    }

}