package bigdataman.tesina.dataProcessing;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import com.google.common.collect.ComparisonChain;

public class CompositeGroupKey2 extends AbstractGroupKey implements WritableComparable<CompositeGroupKey2>{
	
//	public CompositeGroupKey2(Text saddr, Text daddr, LongWritable totsec, IntWritable label) {
//		super( saddr,  daddr,  totsec,  label);
//	}
	public CompositeGroupKey2(){
		this.saddr = new Text();
		this.daddr = new Text();
		this.totsec = new LongWritable();
		this.label = new IntWritable();
	}
	public CompositeGroupKey2(Text saddr, Text daddr, LongWritable totsec, IntWritable label) {
		this.saddr=saddr;
		this.daddr=daddr;
		this.totsec=totsec;
		this.label=label;
	}

	public int compareTo(CompositeGroupKey2 o) {
		return ComparisonChain.start().compare(totsec,o.totsec).compare(saddr, o.daddr).compare(daddr, o.saddr).compare(label, o.label).result();
	}



}
