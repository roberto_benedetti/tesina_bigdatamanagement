package bigdataman.tesina.dataProcessing;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.Text;

import com.google.common.collect.ComparisonChain;
public class CompositeGroupKey extends AbstractGroupKey implements WritableComparable<CompositeGroupKey> {

	public CompositeGroupKey(){
		this.saddr = new Text();
		this.daddr = new Text();
		this.totsec = new LongWritable();
		this.label = new IntWritable();
	}
	
	public CompositeGroupKey(Text saddr, Text daddr, LongWritable totsec, IntWritable label) {
		this.saddr=saddr;
		this.daddr=daddr;
		this.totsec=totsec;
		this.label=label;
	}

	public int compareTo(CompositeGroupKey o) {
		return ComparisonChain.start().compare(saddr, o.saddr).compare(daddr, o.daddr).compare(totsec,o.totsec).compare(label, o.label).result();
	}
}