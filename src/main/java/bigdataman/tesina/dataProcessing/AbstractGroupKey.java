package bigdataman.tesina.dataProcessing;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;

import com.mongodb.BasicDBObject;

public abstract class AbstractGroupKey {
	protected Text saddr;
	protected Text daddr;
	protected LongWritable totsec;
	protected IntWritable label;

	public void write(DataOutput out) throws IOException {
		saddr.write(out);
		daddr.write(out);
		totsec.write(out);
		label.write(out);
		
	}
	public void readFields(DataInput in) throws IOException {
		saddr.readFields(in);
		daddr.readFields(in);
		totsec.readFields(in);
		label.readFields(in);
	}
		
	@Override
	public String toString() {
		return "saddr=" + saddr + ", daddr=" + daddr + ", totsec=" + totsec +", label=" +label;
	}

	public Text getSaddr() {
		return saddr;
	}

	public Text getDaddr() {
		return daddr;
	}
	public IntWritable getLabel() {
		return label;
	}

	public LongWritable getTotsec() {
		return totsec;
	}
	public BasicDBObject getDBObject() {
		BasicDBObject out = new BasicDBObject();
		out.put("saddr", saddr.toString());
		out.put("daddr", daddr.toString());
		out.put("totsec", totsec.get());
		out.put("label", label.get());
		return out;
	}

	
}