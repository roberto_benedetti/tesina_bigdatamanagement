conn = new Mongo();
db = conn.getDB("BDM");


db.intermediateResults.aggregate([
   {
      $lookup:
         {
          from: "intermediateResults",
          $let: { saddr_eq: "$_id.key.saddr", daddr_eq: "$_id.key.daddr", totsec_eq: "$_id.key.totsec", label_eq: "$_id.key.label" },
           pipeline: [
              { $match:
                 { $expr:
                    { $and:
                       [
                         { $eq: [ "$_id.key.daddr","$$saddr_eq"]},
                         { $eq: [ "$_id.key.saddr","$$daddr_eq"]},
			 { $eq: [ "$_id.key.totsec","$$totsec_eq"]},
			 { $eq: [ "$_id.key.label","$$label_eq"]}

                       ]
                    }
                 }
              },
              { $project: { _id: 1 } }
           ],
           as: "featuresIn"
         }
    }
	{$out : "dataset" }
]);
