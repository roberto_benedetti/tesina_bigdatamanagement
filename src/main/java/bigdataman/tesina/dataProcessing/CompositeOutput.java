package bigdataman.tesina.dataProcessing;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.apache.hadoop.io.Writable;

import com.mongodb.BasicDBObject;

@SuppressWarnings("serial")
public class CompositeOutput extends BasicDBObject implements Writable {
	private int numPackets;
	private int totalDataLength;
	private double avgDataLegth;
	private double stdDataLength;
	
	public CompositeOutput(int numPackets, int totalDataLength, double avgDataLegth, double stdDataLength) {
		this.numPackets = numPackets;
		this.totalDataLength = totalDataLength;
		this.avgDataLegth = avgDataLegth;
		this.stdDataLength = stdDataLength;
	}
	public void write(DataOutput out) throws IOException {
		out.writeInt(numPackets);
		out.writeInt(totalDataLength);
		out.writeDouble(avgDataLegth);
		out.writeDouble(stdDataLength);
	}

	public void readFields(DataInput in) throws IOException {
		this.numPackets= in.readInt();
		this.totalDataLength = in.readInt();
		this.avgDataLegth = in.readDouble();
		this.stdDataLength = in.readDouble();
	}
	@Override
	public String toString() {
		return Integer.toString(numPackets);
	}

}
