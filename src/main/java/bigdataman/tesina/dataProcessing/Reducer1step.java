package bigdataman.tesina.dataProcessing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import org.apache.hadoop.io.IntWritable;

import org.apache.hadoop.mapreduce.Reducer;
import org.bson.BSONObject;
import org.bson.BasicBSONObject;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.hadoop.io.BSONWritable;

public class Reducer1step  extends Reducer<CompositeGroupKey, IntWritable, BSONWritable, BSONWritable> {
	// Initialize the couple id - value for mongoDB insert
	private BSONWritable id = new BSONWritable(); 
	private BSONWritable value = new BSONWritable(); 

	private final List<AbstractFeatExpansion> listFeats = Arrays.asList(
			new ExpansionMean(),
			new ExpansionStandardDeviation(),
			new ExpansionTotalDataLength(),
			new ExpansionNumPackets());
	
	@Override	
    public void reduce(CompositeGroupKey key, Iterable<IntWritable> values, Context context)
        throws IOException, InterruptedException {

        ArrayList<Integer> listValues = new ArrayList<Integer>();
        for (final IntWritable value : values) {
        	int val = value.get();
            listValues.add(val);
        }
        BSONObject outDoc = BasicDBObjectBuilder.start().add("key", key.getDBObject()).get();
        id.setDoc(outDoc);
        BasicBSONObject output = new BasicBSONObject();
        
        for (AbstractFeatExpansion abs: listFeats)
        	output.put(abs.getDescriptor(), abs.compute(listValues));
        BSONObject features = new BasicBSONObject();
        features.put("featuresOut", output);
        
        value.setDoc(features);
        context.write(id,value);

    }

}