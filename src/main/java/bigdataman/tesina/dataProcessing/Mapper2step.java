package bigdataman.tesina.dataProcessing;

import java.io.IOException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.bson.BSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.hadoop.io.BSONWritable;


public class Mapper2step extends Mapper<BSONObject, BSONObject, CompositeGroupKey2, BSONWritable> {

	@Override
	public void map(BSONObject key, BSONObject val, Context context)
			throws IOException, InterruptedException {
		BasicDBObject keys = (BasicDBObject) key.get("key");
		String saddr = keys.getString("saddr");
		String daddr = keys.getString("daddr");
		Long totsec = Long.parseLong(keys.getString("totsec"));
		int label = Integer.parseInt(keys.getString("label"));

		BasicDBObject values = (BasicDBObject) val.get("featuresOut");
		CompositeGroupKey2 mapKey = new CompositeGroupKey2(new Text(saddr), new Text(daddr), new LongWritable(totsec), new IntWritable(label));
		context.write(mapKey, new BSONWritable(values)); // da cambiare

	}
}
