package bigdataman.tesina.dataProcessing;

import java.util.ArrayList;


public class ExpansionMean extends AbstractFeatExpansion{

	public ExpansionMean() {
		this.descriptor="mean";
	}

	public double compute(ArrayList<Integer> listValues) {
		double sum=0;
        for (int value : listValues) {
            sum += value;
        }
        return sum/listValues.size();
	}

}
