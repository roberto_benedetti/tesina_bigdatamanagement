package bigdataman.tesina.dataProcessing;

import java.io.IOException;



import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.bson.BSONObject;

public class Mapper1step extends Mapper<Object, BSONObject, CompositeGroupKey, IntWritable> {

	@Override
	public void map(Object key, BSONObject val, Context context)
			throws IOException, InterruptedException {
		
		String saddr = (String) val.get("saddr");
		String daddr = (String) val.get("daddr");
		Long totsec =((Number) val.get("sec")).longValue()*1000000+((Number) val.get("usec")).longValue();
		totsec = Math.floorDiv(totsec,10000); //original 1000
		int label = (Integer) val.get("label");

		CompositeGroupKey mapKey = new CompositeGroupKey(new Text(saddr), new Text(daddr), new LongWritable(totsec), new IntWritable(label));
		context.write(mapKey,new IntWritable((((Number) val.get("datalength")).intValue())));

	}
}