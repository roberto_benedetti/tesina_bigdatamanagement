package bigdataman.tesina.dataProcessing;

import java.util.ArrayList;

public class ExpansionTotalDataLength extends AbstractFeatExpansion{
	public ExpansionTotalDataLength() {
		this.descriptor="totDataLength";
	}

	public double compute(ArrayList<Integer> listValues) {
		double sum=0;
		for (int x: listValues)
			sum+=x;
		return sum;
	}

}
