package bigdataman.tesina.dataProcessing;

import java.util.ArrayList;

public class ExpansionStandardDeviation extends AbstractFeatExpansion{
	public ExpansionStandardDeviation() {
		this.descriptor="std";
	}

	public double compute(ArrayList<Integer> listValues) {
		double std=0;
		double mean = (new ExpansionMean()).compute(listValues);
		for (int value:listValues) {
			std+=Math.pow((value-mean),2);
		}
		return Math.sqrt(std/listValues.size());
		}
}
