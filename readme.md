# DEVELOPMENT OF A SOFTWARE FOR THE PROCESSING OF NETWORK TRAFFIC FEATURING DISTRIBUTED TECHNOLOGIES

Project work for the exam of: Big Data Management. The software processes packets sniffed from a tcp traffic creating a dataset ready for network analysis techniques.

It works using Hadoop MapReduce for computations and MongoDB as storage. Full read of the work can be found below.

// 

Progetto di tesina per l'esame di: Big Data Management. Il software processa paccketti sniffati da un traffico tcp creando un dataset pronto per tecniche di analisi.

Il funzionamento � basato su Hadoop MapReduce per la parte di calcolo e MongoDB per lo storage. Documentazione completa sul lavoro in fondo alla pagina.

[Documentation (italian)](https://bitbucket.org/roberto_benedetti/tesina_bigdatamanagement/raw/227c472eed34db9a3ceb7568272c5d5977b46673/Tesina_BigDataManagement.pdf)
